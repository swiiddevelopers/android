package swiidappdesigns.antz;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ShaldorTheGreat on 5/2/2018.
 */

public class AdapterRecycleSubCat extends RecyclerView.Adapter<AdapterRecycleSubCat.ViewHolder>{
    private ArrayList<String> availableSubCat = new ArrayList<>();
    private ArrayList<String> availableImgURLs = new ArrayList<>();
    private Context mContext;

    public AdapterRecycleSubCat(ArrayList<String> availableSubCat, ArrayList<String> availableImgURLs, Context mContext) {
        this.availableSubCat = availableSubCat;
        this.availableImgURLs = availableImgURLs;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_events_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(mContext).asBitmap().load(availableImgURLs.get(position)).into(holder.image);
        holder.name.setText(availableSubCat.get(position));
    }

    @Override
    public int getItemCount() {
        return availableSubCat.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView image;
        TextView name;
        Button addToFav;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imageViewCurrCat);
            name = itemView.findViewById(R.id.textViewCurrCatName);
            addToFav = itemView.findViewById(R.id.buttonAddCurrCatToFav);
        }
    }
}
