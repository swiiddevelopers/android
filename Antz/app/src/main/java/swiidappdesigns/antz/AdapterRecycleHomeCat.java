package swiidappdesigns.antz;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ShaldorTheGreat on 5/14/2018.
 */

public class AdapterRecycleHomeCat extends RecyclerView.Adapter<AdapterRecycleHomeCat.ViewHolder> {
    private ArrayList<String> availableHomeCat = new ArrayList<>();
    private ArrayList<Integer> availableHomeCatPic = new ArrayList<>();

    private Context mContext;

    String sItem;

    public AdapterRecycleHomeCat(ArrayList<String> availableHomeCat, ArrayList<Integer> availableHomeCatPic, Context mContext) {
        this.availableHomeCat = availableHomeCat;
        this.availableHomeCatPic = availableHomeCatPic;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AdapterRecycleHomeCat.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_home_cat, parent, false);
        return new AdapterRecycleHomeCat.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecycleHomeCat.ViewHolder holder, int position) {
        Glide.with(mContext).asBitmap().load(availableHomeCatPic.get(position)).into(holder.image);
        holder.name.setText(availableHomeCat.get(position));
    }

    @Override
    public int getItemCount() {
        return availableHomeCat.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView name;
        CircleImageView image;

        public ViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.textViewHomeCat);
            image = itemView.findViewById(R.id.imageViewHomeCat);
            image.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(mContext, "Image Selected", Toast.LENGTH_SHORT).show();
            sItem = availableHomeCat.get(getLayoutPosition());
            Intent intent = new Intent("home-cat-item");
            intent.putExtra("homeCatSelected",sItem);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        }
    }
}
