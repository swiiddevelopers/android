package swiidappdesigns.antz;

/**
 * Created by ShaldorTheGreat on 5/7/2018.
 */

public class InfoPurchaseSeeds {
    public String seedAmounts, seedPrice;

    public InfoPurchaseSeeds(String seedAmounts,String seedPrice){
        this.seedAmounts = seedAmounts;
        this.seedPrice = seedPrice;
    }
}
