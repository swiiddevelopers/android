package swiidappdesigns.antz;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ShaldorTheGreat on 5/2/2018.
 */

public class AdapterRecycleOnboardingSubCat extends RecyclerView.Adapter<AdapterRecycleOnboardingSubCat.ViewHolder>{
    private ArrayList<String> availableSubCat = new ArrayList<>();
    //private ArrayList<String> availableImgURLs = new ArrayList<>();
    private Context mContext;

    String sItem;

    public AdapterRecycleOnboardingSubCat(ArrayList<String> availableSubCat, /*ArrayList<String> availableImgURLs,*/ Context mContext) {
        this.availableSubCat = availableSubCat;
        //this.availableImgURLs = availableImgURLs;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_main_cat_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //Glide.with(mContext).asBitmap().load(availableImgURLs.get(position)).into(holder.image);
        holder.name.setText(availableSubCat.get(position));
    }

    @Override
    public int getItemCount() {
        return availableSubCat.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        //CircleImageView image;
        TextView name;
        Button addToFav;

        public ViewHolder(View itemView) {
            super(itemView);
            //image = itemView.findViewById(R.id.imageViewCurrCat);
            name = itemView.findViewById(R.id.textViewMainCatName);
            addToFav = itemView.findViewById(R.id.buttonAddMainCatToFav);
            addToFav.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
                sItem = availableSubCat.get(getLayoutPosition());
                addToFav.setBackgroundResource(R.drawable.ic_favorite_selected);
                Intent intent = new Intent("custom-message-sub");
                intent.putExtra("subCat",sItem);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        }
    }
}
