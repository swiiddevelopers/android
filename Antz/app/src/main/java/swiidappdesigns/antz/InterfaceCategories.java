package swiidappdesigns.antz;

/**
 * Created by ShaldorTheGreat on 5/4/2018.
 */

public interface InterfaceCategories {

    public static final String[] sMainCategories = {"Indoor Events", "Observation Events",
    "Competitive Events","Collection Events","Outdoor Events"};

    public static final String[] sIndoorEvents = {"3D printing","Acting","Amateur Radio",
    "Aquascaping","Baking","Baton Twirling","Board/tabletop Games","Book Restoration","Cabaret",
    "Calligraphy","Candle Making","Coffee Roasting","Coloring","Computer Programming",
    "Cooking","Cosplaying","Couponing","Creative Writing","Crocheting","Cross-stitch"};

    public static final String[] sObservationEvents = {"Fishkeeping","Learning","Microscopy",
            "Reading","Shortwave Listening","Videophilia","Aircraft Spotting","Amateur Astronomy",
            "Astrology", "Birdwatching","Bus Spotting","Geocaching","Gongoozling", "Herping",
            "Hiking/backpacking","Meteorology","Photography","Satellite Watching","Trainspotting",
            "Traveling","Whale Watching"};

    public static final String[] sCompetitiveEvents = {"Animal Fancy","Badminton","Baton Twirling",
            "Billiards","Bowling","Boxing","Bridge","Cheerleading", "Chess", "Color Guard","Curling",
            "Dancing","Darts", "Debate","ESports","Fencing", "Go", "Gymnastics","Ice Skating",
            "Kabaddi"};

    public static final String[] sCollectionEvents = {"Action figure","Antiquing","Art Collecting",
            "Book Collecting","Card Collecting","Coin Collecting","Comic Book Collecting",
            "Deltiology (postcard collecting)", "Die-cast Toy", "Element Collecting",
            "Movie and Movie Memorabilia Collecting", "Record Collecting",
            "Shoes/Dress Collecting", "Stamp Collecting","Video Game Collecting","Vintage Cars",
            "Antiquities", "Auto Audiophilia","Flower Collecting and Pressing", "Fossil Hunting"};

    public static final String[] sOutdoorEvents = {"Air Sports","Archery","Astronomy",
            "BASE Jumping","Baseball","Basketball","Beekeeping", "Bird Watching", "Blacksmithing",
            "Board Sports", "Bodybuilding", "Brazilian Jiu-jitsu", "Camping", "Canyoning","Dowsing",
            "Driving", "Fishing", "Flag Football","Flying", "Flying disc"};
}
