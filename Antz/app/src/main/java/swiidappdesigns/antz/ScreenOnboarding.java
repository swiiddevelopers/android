package swiidappdesigns.antz;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ScreenOnboarding extends AppCompatActivity {

    EditText edtUserName, edtUserEmail, edtUserPhone;

    DatePicker datePicker;

    CircleImageView imageViewProfilePic;

    private Uri resultOnboardUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_onboarding);

        edtUserName = findViewById(R.id.editTextOnboardName);
        edtUserEmail = findViewById(R.id.editTextOnboardEmail);
        edtUserPhone = findViewById(R.id.editTextOnboardPhone);

        datePicker = findViewById(R.id.datePicker);

        imageViewProfilePic = findViewById(R.id.imageViewSetUserProfilePic);

        imageViewProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent,2);
            }
        });

        setAllUserInfoAlreadyInDB();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 2 && resultCode == Activity.RESULT_OK){
            final Uri imageUri = data.getData();
            resultOnboardUri = imageUri;
            imageViewProfilePic.setImageURI(resultOnboardUri);
        }
    }

    private void setAllUserInfoAlreadyInDB(){
        final DatabaseReference userDetFound = Constants.myUsersPerDetRef;
        userDetFound.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String value = snapshot.getKey().toString();

                        if(value.equals("profileName")){
                            String valueFound = snapshot.getValue().toString();
                            edtUserName.setText(valueFound);
                        }else if(value.equals("email")){
                            String valueFound = snapshot.getValue().toString();
                            edtUserEmail.setText(valueFound);
                        }else if(value.equals("phoneNumber")){
                            String valueFound = snapshot.getValue().toString();
                            edtUserPhone.setText(valueFound);
                        }else if(value.equals("DOB")){
                            String valueFound = snapshot.getValue().toString();
                            int day,month,year;
                            String[] splitDOB = valueFound.split("\\.");
                            day = Integer.parseInt(splitDOB[0]);
                            month = Integer.parseInt(splitDOB[1]);
                            year = Integer.parseInt(splitDOB[2]);
                            datePicker.updateDate(year,(month-1),day);
                        }else if(value.equals("profilePic")){
                            String valueFound = snapshot.getValue().toString();
                            Glide.with(getApplication()).load(valueFound).into(imageViewProfilePic);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    private void saveAllUserPersonalDetToDb(){
        String email,phoneNo,userName,DOB;
        int day,month,year;

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String uid = user.getUid();

        Map<String, Object> addingPersonalDetToDB = new HashMap<>();
        Map<String, Object> addingOnboardingToDB = new HashMap<>();

        email = edtUserEmail.getText().toString();
        phoneNo = edtUserPhone.getText().toString();
        userName = edtUserName.getText().toString();
        day = datePicker.getDayOfMonth();
        month = datePicker.getMonth() + 1;
        year = datePicker.getYear();
        DOB = day+"."+month+"."+year;

        addingPersonalDetToDB.put("profileName",userName);
        addingPersonalDetToDB.put("email",email);
        addingPersonalDetToDB.put("phoneNumber",phoneNo);
        addingPersonalDetToDB.put("DOB",DOB);

        Constants.myUsersPerDetRef.updateChildren(addingPersonalDetToDB);

        //The following sets the image of the user to the db
        if(resultOnboardUri != null) {
            StorageReference filePath = FirebaseStorage.getInstance().getReference()
                    .child("profile_images")
                    .child(uid);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap
                        (getApplication().getContentResolver(), resultOnboardUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
            byte[] data = baos.toByteArray();
            UploadTask uploadTask = filePath.putBytes(data);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    Map newImage = new HashMap();
                    newImage.put("profilePic", downloadUrl.toString());

                    Constants.myUsersPerDetRef.updateChildren(newImage);
                }
            });
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    finish();
                    return;
                }
            });
        }

        addingOnboardingToDB.put("Onboarding","true");
        Constants.myUserIDRef.updateChildren(addingOnboardingToDB);
    }

    public void userCreateAccountSelected(View view){
        boolean areAllFieldsEntered = checkAllFields();
        if(areAllFieldsEntered == false){
            AlertDialog.Builder builder = new AlertDialog.Builder(ScreenOnboarding.this);
            builder.setMessage("Please note that you have not filled in all the required information"
                    + ". Please see which sections still need to be filled in.")
                    .setPositiveButton("I understand", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }else{
            saveAllUserPersonalDetToDb();
            startActivity(new Intent(ScreenOnboarding.this, ScreenCategories.class));
        }
    }

    private boolean checkAllFields(){
        boolean allGood = true;
        if(edtUserEmail.getText().toString().equals("")){
            edtUserEmail.setText("");
            edtUserEmail.setHintTextColor(Color.RED);
            edtUserEmail.setHint("Please fill in your Email here");
            allGood = false;
        }
        if(edtUserName.getText().toString().equals("")){
            edtUserName.setText("");
            edtUserName.setHintTextColor(Color.RED);
            edtUserName.setHint("Please fill in your Name and Last Name here");
            allGood = false;
        }
        if(edtUserPhone.getText().toString().equals("")){
            edtUserPhone.setText("");
            edtUserPhone.setHintTextColor(Color.RED);
            edtUserPhone.setHint("Please fill in your Phone No. here");
            allGood = false;
        }
        return allGood;
    }
}
