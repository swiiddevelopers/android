package swiidappdesigns.antz;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ShaldorTheGreat on 5/29/2018.
 */

public class AdapterRecycleUsersInLevels extends RecyclerView.Adapter<AdapterRecycleUsersInLevels.ViewHolder> {
    private ArrayList<String> availableUsersName = new ArrayList<>();
    private ArrayList<String> availableUserImg = new ArrayList<>();
    private ArrayList<String> availableUserPoints = new ArrayList<>();
    private Context mContext;

    int f = 0;

    public AdapterRecycleUsersInLevels(ArrayList<String> availableUsersName,
                                       ArrayList<String> availableUserImg,
                                       ArrayList<String> availableUserPoints,
                                       Context mContext) {
        this.availableUsersName = availableUsersName;
        this.availableUserImg = availableUserImg;
        this.availableUserPoints = availableUserPoints;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AdapterRecycleUsersInLevels.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_leaderboard_user, parent, false);
        return new AdapterRecycleUsersInLevels.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecycleUsersInLevels.ViewHolder holder, int position) {
        Glide.with(mContext).asBitmap().load(availableUserImg.get(position)).into(holder.image);
        holder.name.setText(availableUsersName.get(position));
        holder.points.setText(availableUserPoints.get(position));
    }

    @Override
    public int getItemCount() {
        return availableUsersName.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView name, points;
        CircleImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.textViewLeaderboardUserName);
            points = itemView.findViewById(R.id.textViewLeaderboardUserPoints);
            image = itemView.findViewById(R.id.imageViewLeaderboardUserItem);
            image.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }

    }
}
