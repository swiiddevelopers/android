package swiidappdesigns.antz;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ShaldorTheGreat on 5/2/2018.
 */

public class AdapterRecycleHighlights extends RecyclerView.Adapter<AdapterRecycleHighlights.ViewHolder>{
    private ArrayList<String> availableSubCatHighlights = new ArrayList<>();
    private ArrayList<String> availableHighlightImgURLs = new ArrayList<>();
    private Context mContext;

    public AdapterRecycleHighlights(ArrayList<String> availableSubCat, ArrayList<String> availableImgURLs, Context mContext) {
        this.availableSubCatHighlights = availableSubCat;
        this.availableHighlightImgURLs = availableImgURLs;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_hightlights_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(mContext).asBitmap().load(availableHighlightImgURLs.get(position)).into(holder.image);
        holder.name.setText(availableSubCatHighlights.get(position));
    }

    @Override
    public int getItemCount() {
        return availableSubCatHighlights.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView image;
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imageViewCurrHighlights);
            name = itemView.findViewById(R.id.textViewCurrCatName);
        }
    }
}
