package swiidappdesigns.antz;

/**
 * Created by ShaldorTheGreat on 5/7/2018.
 */

public interface InterfacePurchaseSeeds {

    public static final String[] seedsAmounts = {"5 Seeds", "15 Seeds", "45 Seeds","135 Seeds"};

    public static final String[] seedsPrices = {"R10.00", "R30.00", "R90.00","R270.00"};

}
