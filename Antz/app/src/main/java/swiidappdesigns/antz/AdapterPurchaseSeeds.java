package swiidappdesigns.antz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ShaldorTheGreat on 5/7/2018.
 */

public class AdapterPurchaseSeeds extends ArrayAdapter<InfoPurchaseSeeds> {
    public AdapterPurchaseSeeds(Context context, ArrayList<InfoPurchaseSeeds> infoPurchaseSeeds){
        super(context,0,infoPurchaseSeeds);
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        final InfoPurchaseSeeds infoPurchaseSeeds = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_layout_purchase_seeds,
                    parent, false);
        }

        TextView seedAmount = convertView.findViewById(R.id.textViewSeedsAmount);
        TextView seedPrice = convertView.findViewById(R.id.textViewSeedsPrice);

        seedAmount.setText(infoPurchaseSeeds.seedAmounts);
        seedPrice.setText(infoPurchaseSeeds.seedPrice);



        return convertView;
    }

}
