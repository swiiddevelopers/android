package swiidappdesigns.antz;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ShaldorTheGreat on 5/15/2018.
 */

public class AdapterRecycleMainCat extends RecyclerView.Adapter<AdapterRecycleMainCat.ViewHolder> {

    private ArrayList<String> availableMainCat = new ArrayList<>();
    String[] arrayMainCatItemSelected = new String[2];
    private Context mContext;

    String sItem;

    int f = 0;

    public AdapterRecycleMainCat(ArrayList<String> availableMainCat, Context mContext) {
        this.availableMainCat = availableMainCat;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AdapterRecycleMainCat.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_main_cat_item, parent, false);
        return new AdapterRecycleMainCat.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecycleMainCat.ViewHolder holder, int position) {
        holder.name.setText(availableMainCat.get(position));
    }

    @Override
    public int getItemCount() {
        return availableMainCat.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView name;
        Button addToFav;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.textViewMainCatName);
            addToFav = itemView.findViewById(R.id.buttonAddMainCatToFav);
            addToFav.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(f<2){
                sItem = availableMainCat.get(getLayoutPosition());
                arrayMainCatItemSelected[f] = String.valueOf(getLayoutPosition());
                addToFav.setBackgroundResource(R.drawable.ic_favorite_selected);
                Intent intent = new Intent("custom-message");
                intent.putExtra("mainCat",sItem);
                intent.putExtra("mainCatDone","false");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                f++;
            }else if(f == 2){
                sItem = availableMainCat.get(getLayoutPosition());
                Intent intent = new Intent("custom-message");
                intent.putExtra("mainCat",sItem);
                intent.putExtra("mainCatDone","true");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                f=0;
            }
        }

    }
}
