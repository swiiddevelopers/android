package swiidappdesigns.antz;

import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.auth.ResultCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScreenSplash extends AppCompatActivity {

    //Sets up the firebase database reference
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myUsersRef = database.getReference("Users");

    private static final int RC_SIGN_IN = 123;

    // Choose authentication providers
    List<AuthUI.IdpConfig> providers = Arrays.asList(
            new AuthUI.IdpConfig.EmailBuilder().build(),
            new AuthUI.IdpConfig.PhoneBuilder().build(),
            new AuthUI.IdpConfig.GoogleBuilder().build());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_splash);
        setStatusBarTranslucent(true);

        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {}

            public void onFinish() {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                if (auth.getCurrentUser() != null) {
                    // already signed in
                    startActivity(new Intent(ScreenSplash.this, ScreenHome.class));
                }else{
                    // Create and launch sign-in intent for a user
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setAvailableProviders(providers)
                                    .setTheme(R.style.MyTheme)
                                    .build(),
                            RC_SIGN_IN);
                }
            }
        }.start();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == ResultCodes.OK) {
                setUpBasicInfo();
                startActivity(new Intent(ScreenSplash.this, ScreenHome.class));
            } else {
                // Sign in failed, check response for error code
                // ...
            }
        }
    }

    protected void setUpBasicInfo(){
        Map<String, Object> addingPersonalDetToDB = new HashMap<>();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            String uid = user.getUid();
            String name = user.getDisplayName();
            String email = user.getEmail();
            String phoneNumber = user.getPhoneNumber();
            Uri photoUrl = user.getPhotoUrl();
            String provider = user.getProviderId();

            if(phoneNumber == "" || phoneNumber == null){
                addingPersonalDetToDB.put("phoneNumber","");
            }else{
                addingPersonalDetToDB.put("phoneNumber",phoneNumber);
            }
            addingPersonalDetToDB.put("profileName",name);
            addingPersonalDetToDB.put("email",email);
            addingPersonalDetToDB.put("provider",provider);
            myUsersRef.child(uid).child("PersonalDet").updateChildren(addingPersonalDetToDB);
        }
    }

    //Method that sets the status bar to translucent
    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }
}
