package swiidappdesigns.antz;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ShaldorTheGreat on 5/29/2018.
 */

public class AdapterRecycleLeaderboardLevels extends RecyclerView.Adapter<AdapterRecycleLeaderboardLevels.ViewHolder> {
    private ArrayList<String> availableLevels = new ArrayList<>();
    private ArrayList<String> availableLevelImgs = new ArrayList<>();
    private Context mContext;

    String sItem;

    int f = 0;

    public AdapterRecycleLeaderboardLevels(ArrayList<String> availableLevels, ArrayList<String> availableLevelImgs, Context mContext) {
        this.availableLevels = availableLevels;
        this.availableLevelImgs = availableLevelImgs;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AdapterRecycleLeaderboardLevels.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_leaderboard, parent, false);
        return new AdapterRecycleLeaderboardLevels.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecycleLeaderboardLevels.ViewHolder holder, int position) {
        Glide.with(mContext).asBitmap().load(availableLevelImgs.get(position)).into(holder.image);
        holder.name.setText(availableLevels.get(position));
    }

    @Override
    public int getItemCount() {
        return availableLevels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView name;
        CircleImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.textViewLeaderboardItem);
            image = itemView.findViewById(R.id.imageViewLeaderboardItem);
            image.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            sItem = availableLevels.get(getLayoutPosition());
            Intent intent = new Intent("leaderboard-level");
            intent.putExtra("levelSelected",sItem);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        }

    }
}
