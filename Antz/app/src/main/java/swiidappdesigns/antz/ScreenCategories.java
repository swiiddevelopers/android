package swiidappdesigns.antz;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ScreenCategories extends AppCompatActivity {
    private ArrayList<String> arrayListMainCat = new ArrayList<>();
    private ArrayList<String> arrayListSubCat = new ArrayList<>();

    private String[] arrayMainCatSelected = new String[3];
    private String[] arraySubCatSelected = new String[3];
    private String[][] arrayUserSelectedCategories = new String[3][3];

    AdapterRecycleMainCat adapter;
    AdapterRecycleOnboardingSubCat adapterSub;

    int f = 0;
    int g = 0;
    int h = 0;

    CardView crdVwMainCat, crdVwSubCat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_categories);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        crdVwMainCat = findViewById(R.id.cardViewMainCat);
        crdVwSubCat = findViewById(R.id.cardViewSubCat);

        AlertDialog.Builder builder = new AlertDialog.Builder(ScreenCategories.this);
        builder.setCancelable(true);
        builder.setMessage("Welcome to Antz!\n\n" +
                "Before you can continue we need to learn a little more about you.\n\n" +
                "Please select your top 3 Main Categories by selecting the heart icon.")
                .setPositiveButton("Will Do!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {}
                });
        AlertDialog dialog = builder.create();
        dialog.show();

        loadMainCat();
        initRecyclerViewMainCat();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-message"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mSubMessageReceiver,
                new IntentFilter("custom-message-sub"));
    }

    private void loadMainCat(){
        for(int i = 0; i < InterfaceCategories.sMainCategories.length;i++){
            arrayListMainCat.add(InterfaceCategories.sMainCategories[i]);
        }
    }
    private void loadSubCat(){
        addingSubsToList(arrayMainCatSelected[h]);
        Toast.makeText(this,String.valueOf(arrayMainCatSelected[h]),Toast.LENGTH_SHORT).show();
        initRecyclerViewSubCat();
    }
    private void addingSubsToList(String mainCat){
        arrayListSubCat.clear();
        if(mainCat.equals("Indoor Events")){
            for(int j = 0; j< InterfaceCategories.sIndoorEvents.length; j++){
                arrayListSubCat.add(InterfaceCategories.sIndoorEvents[j]);
            }
        }else if(mainCat.equals("Observation Events")){
            for(int j = 0; j< InterfaceCategories.sObservationEvents.length; j++){
                arrayListSubCat.add(InterfaceCategories.sObservationEvents[j]);
            }
        }else if(mainCat.equals("Competitive Events")){
            for(int j = 0; j< InterfaceCategories.sCompetitiveEvents.length; j++){
                arrayListSubCat.add(InterfaceCategories.sCompetitiveEvents[j]);
            }
        }else if(mainCat.equals("Collection Events")){
            for(int j = 0; j< InterfaceCategories.sCollectionEvents.length; j++){
                arrayListSubCat.add(InterfaceCategories.sCollectionEvents[j]);
            }
        }else if(mainCat.equals("Outdoor Events")){
            for(int j = 0; j< InterfaceCategories.sOutdoorEvents.length; j++){
                arrayListSubCat.add(InterfaceCategories.sOutdoorEvents[j]);
            }
        }
    }

    private void initRecyclerViewMainCat(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        RecyclerView recyclerView = findViewById(R.id.recyclerViewMainCat);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AdapterRecycleMainCat(arrayListMainCat,this);
        recyclerView.setAdapter(adapter);
    }
    private void initRecyclerViewSubCat(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        RecyclerView recyclerView = findViewById(R.id.recyclerViewSubCat);
        recyclerView.setLayoutManager(layoutManager);
        adapterSub = new AdapterRecycleOnboardingSubCat(arrayListSubCat,this);
        recyclerView.setAdapter(adapterSub);
    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //This section is for selecting main categories
            String sItemName = intent.getStringExtra("mainCat");
            String mainCatSelectionDone = intent.getStringExtra("mainCatDone");

            arrayMainCatSelected[f] = sItemName;
            f++;

            if(mainCatSelectionDone.equals("true")) {
                Map<String, Object> addingMainCatToDB = new HashMap<>();
                for(int i = 0; i<=f; i++){
                    if(i==0){
                        addingMainCatToDB.put(Constants.arrayMainCatDB[i], arrayMainCatSelected[i]);
                        DatabaseReference addingMainCatRef = Constants.myUsersinterestsRef;
                        addingMainCatRef.updateChildren(addingMainCatToDB);
                    }else if(i==1){
                        addingMainCatToDB.put(Constants.arrayMainCatDB[i], arrayMainCatSelected[i]);
                        DatabaseReference addingMainCatRef = Constants.myUsersinterestsRef;
                        addingMainCatRef.updateChildren(addingMainCatToDB);
                    }else if(i==2){
                        addingMainCatToDB.put(Constants.arrayMainCatDB[i], arrayMainCatSelected[i]);
                        DatabaseReference addingMainCatRef = Constants.myUsersinterestsRef;
                        addingMainCatRef.updateChildren(addingMainCatToDB);
                    }
                }
                f=0;
                crdVwMainCat.setVisibility(View.GONE);
                crdVwSubCat.setVisibility(View.VISIBLE);
                AlertDialog.Builder builder = new AlertDialog.Builder(ScreenCategories.this);
                builder.setCancelable(true);
                builder.setMessage("Great!\n\n" +
                        "Now you can select 1 Sub Category of each Main Category you selected.")
                        .setPositiveButton("Awesome!", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                loadSubCat();
                initRecyclerViewSubCat();
            }
        }
    };

    public BroadcastReceiver mSubMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //This section is for selecting sub categories
            String sSubItemName = intent.getStringExtra("subCat");
            arraySubCatSelected[g] = sSubItemName;

            Map<String, Object> addingSubCatToDB = new HashMap<>();
            for(int m = 0; m<=g; m++){
                if(arrayMainCatSelected[m].equals("Indoor Events")){
                    addingSubCatToDB.put("interestSubCatIE1", arraySubCatSelected[m]);
                }else if(arrayMainCatSelected[m].equals("Observation Events")) {
                    addingSubCatToDB.put("interestSubCatOBE1", arraySubCatSelected[m]);
                }else if(arrayMainCatSelected[m].equals("Collection Events")) {
                    addingSubCatToDB.put("interestSubCatCollE1", arraySubCatSelected[m]);
                }else if(arrayMainCatSelected[m].equals("Competitive Events")) {
                    addingSubCatToDB.put("interestSubCatCompE1", arraySubCatSelected[m]);
                }else if(arrayMainCatSelected[m].equals("Outdoor Events")) {
                    addingSubCatToDB.put("interestSubCatOE1", arraySubCatSelected[m]);
                }
                DatabaseReference addingSubCatRef = Constants.myUsersinterestsRef;
                addingSubCatRef.updateChildren(addingSubCatToDB);
            }

                if(g==0){
                    arrayUserSelectedCategories[g][g] = arrayMainCatSelected[g];
                    arrayUserSelectedCategories[g][g+1] = arraySubCatSelected[g];
                }else if(g==1){
                    arrayUserSelectedCategories[g][g-1] = arrayMainCatSelected[g];
                    arrayUserSelectedCategories[g][g] = arraySubCatSelected[g];
                }else if(g==2){
                    arrayUserSelectedCategories[g][g-2] = arrayMainCatSelected[g];
                    arrayUserSelectedCategories[g][g-1] = arraySubCatSelected[g];
                    g=0;
                    String message = "";
                    for(int i = 0; i<=arrayUserSelectedCategories.length;i++){
                        if(i == 0){
                            message += "Main Category:\n" + " -" + arrayUserSelectedCategories[i][i] +"\n";
                            message += "  Sub Category:\n" + "   -" + arrayUserSelectedCategories[i][i+1] + "\n";
                        }else if(i == 1){
                            message += "Main Category:\n" + " -" + arrayUserSelectedCategories[i][i-1] +"\n";
                            message += "  Sub Category:\n" + "   -" + arrayUserSelectedCategories[i][i] + "\n";
                        }else if(i == 2){
                            message += "Main Category:\n" + " -" + arrayUserSelectedCategories[i][i-2] +"\n";
                            message += "  Sub Category:\n" + "   -" + arrayUserSelectedCategories[i][i-1] + "\n";
                        }
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(ScreenCategories.this);
                    builder.setCancelable(true);
                    builder.setMessage("Great!\n\n" +
                            "You have selected the following:\n\n"+message)
                            .setPositiveButton("Awesome!", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    startActivity(new Intent(ScreenCategories.this, ScreenHome.class));
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
            }
            g++;
            h++;
            if(h<3){
                loadSubCat();
            }else{
                h=0;
            }

        }
    };
}
