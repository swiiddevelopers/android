package swiidappdesigns.antz;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by ShaldorTheGreat on 5/21/2018.
 */

public interface Constants {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

    //The following are constant Database References
    public static final DatabaseReference myUsersOnboardingRef = database.getReference("Users")
            .child(userId)
            .child("Onboarding");
    public static final DatabaseReference myUsersRef = database.getReference("Users");
    public static final DatabaseReference myUserIDRef = database.getReference("Users")
            .child(userId);
    public static final DatabaseReference myUsersPerDetRef = database.getReference("Users")
            .child(userId)
            .child("userPersonalDet");
    public static final DatabaseReference myUsersLevelRef = database.getReference("Users")
            .child(userId)
            .child("userLevel");
    public static final DatabaseReference myUsersCreditBalRef = database.getReference("Users")
            .child(userId)
            .child("userCreditBalance");
    public static final DatabaseReference myUsersinterestsRef = database.getReference("Users")
            .child(userId)
            .child("userInterests");
    public static final DatabaseReference leaderboardGeneralRef = database.getReference("Leaderboard");

    //The following are for constant string arrays
    public static final String[] arrayLevels={"Hiding Under A Rock",
            "Seeing Light for the First Time","Food Gatherer","Worker Ant","Queen Ant"};
    public static final String[] arrayMainCatDB={"interestMainCat1", "interestMainCat2",
            "interestMainCat3","interestMainCat4","interestMainCat5"};
    public static final String[] arrayMainCatNames={"Indoor Events","Observation Events",
            "Competitive Events","Collection Events","Outdoor Events"};
}
