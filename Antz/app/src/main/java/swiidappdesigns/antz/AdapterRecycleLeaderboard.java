package swiidappdesigns.antz;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ShaldorTheGreat on 5/14/2018.
 */

public class AdapterRecycleLeaderboard extends RecyclerView.Adapter<AdapterRecycleLeaderboard.ViewHolder> {
    private ArrayList<String> availableLeaderboard = new ArrayList<>();
    private ArrayList<String> availableLeaderboardPic = new ArrayList<>();

    private Context mContext;

    String sItem;

    public AdapterRecycleLeaderboard(ArrayList<String> availableLeaderboard,ArrayList<String> availableLeaderboardPic, Context mContext) {
        this.availableLeaderboard = availableLeaderboard;
        this.availableLeaderboardPic = availableLeaderboardPic;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AdapterRecycleLeaderboard.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_leaderboard, parent, false);
        return new AdapterRecycleLeaderboard.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecycleLeaderboard.ViewHolder holder, int position) {
        Glide.with(mContext).asBitmap().load(availableLeaderboardPic.get(position)).into(holder.image);
        holder.name.setText(availableLeaderboard.get(position));
    }

    @Override
    public int getItemCount() {
        return availableLeaderboard.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView name;
        CircleImageView image;

        public ViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.textViewLeaderboardItem);
            image = itemView.findViewById(R.id.imageViewLeaderboardItem);
            image.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            sItem = availableLeaderboard.get(getLayoutPosition());
            Intent intent = new Intent("leaderboard-item");
            intent.putExtra("leaderboardSelected",sItem);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        }
    }
}
