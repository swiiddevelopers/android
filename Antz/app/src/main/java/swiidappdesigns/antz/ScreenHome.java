package swiidappdesigns.antz;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ScreenHome extends AppCompatActivity {

    private ArrayList<String> availableSubCat = new ArrayList<>();
    private ArrayList<String> availableImgURLs = new ArrayList<>();
    private ArrayList<String> availableLevels = new ArrayList<>();
    private ArrayList<String> availableLevelsImgURLs = new ArrayList<>();
    private ArrayList<String> availableLeaderboardItems = new ArrayList<>();
    private ArrayList<String> availableLeaderboardItemsImgURLs = new ArrayList<>();
    private ArrayList<String> availableUserName = new ArrayList<>();
    private ArrayList<String> availableUserImgURLs = new ArrayList<>();
    private ArrayList<String> availableUserPoints = new ArrayList<>();
    private ArrayList<String> availableUserOwnedMainCat = new ArrayList<>();
    private ArrayList<String> availableUserOwnedMainCatImg = new ArrayList<>();
    private ArrayList<String> availableHomeCat = new ArrayList<>();
    private ArrayList<Integer> availableHomeCatImgURL = new ArrayList<>();


    View panelHome, panelProfile, panelSeeds, panelLeaderboard, panelSelectCategories,
            panelHomeCatOwned, panelHomeCatNotOwned;

    CardView crdVwLeaderboard, crdVwLeaderboardUsers;

    Button btnBackToLeaderboard;

    TextView txtDispLeaderboardList, txtDispLeaderboardLevelsList, txtDispUserName, txtUserLevel,
    txtUserSeeds;

    CircleImageView imgUserProfilePic;

    BottomNavigationView botNavView;

    AdapterPurchaseSeeds adapter;
    InfoPurchaseSeeds newSeedInfo;

    private LinearLayoutManager layoutManagerLeaderboard, layoutManagerLeaderboardLevels,
            layoutManagerUserInLevels, layoutManagerSelectMainCat, layoutManagerHomeCat;

    private RecyclerView recyclerViewLeaderboard, recyclerViewLeaderboardLevels,
            recyclerViewUsersInLevels, recyclerViewSelectMainCat, recyclerViewHomeCat;

    private AdapterRecycleLeaderboardLevels adapterLeaderboardLevels;
    private AdapterRecycleLeaderboard adapterLeaderboard;
    private AdapterRecycleUsersInLevels adapaterUsersInLevels;
    private AdapterRecycleSelectMainCat adapaterSelectMainCat;
    private AdapterRecycleHomeCat adapaterHomeCat;

    String sItemName, sItemNameLevel, userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadStaticImagesAndNames();
        initRecyclerViewSubCat();
        loadStaticImagesAndNames();
        initRecyclerViewHighlights();

        panelHome = findViewById(R.id.panelHomeContent);
        panelProfile = findViewById(R.id.panelProfileContent);
        panelSeeds = findViewById(R.id.panelSeeds);
        panelLeaderboard = findViewById(R.id.panelLeaderboard);
        panelSelectCategories = findViewById(R.id.panelSelectCategories);
        panelHomeCatOwned = findViewById(R.id.panelOwnedCat);
        panelHomeCatNotOwned = findViewById(R.id.panelNotOwnedCat);

        crdVwLeaderboard = findViewById(R.id.cardViewLeaderboard);
        crdVwLeaderboardUsers = findViewById(R.id.cardViewLeaderboardUsers);

        btnBackToLeaderboard = findViewById(R.id.buttonBackToLeaderboard);

        txtDispLeaderboardList = findViewById(R.id.textViewDispLeaderboard);
        txtDispLeaderboardLevelsList = findViewById(R.id.textViewDispLeaderboardUsers);
        txtDispUserName = findViewById(R.id.textViewUserName);
        txtUserLevel = findViewById(R.id.textViewUserLevel);
        txtUserSeeds = findViewById(R.id.textViewUserCreditBal);

        imgUserProfilePic = findViewById(R.id.imageViewUserAvatar);

        botNavView = findViewById(R.id.top_nav_user);
        botNavView.setSelectedItemId(R.id.menu_user_home);
        botNavView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_user_home:
                                panelHome.setVisibility(View.VISIBLE);
                                panelProfile.setVisibility(View.GONE);
                                panelSeeds.setVisibility(View.GONE);
                                panelLeaderboard.setVisibility(View.GONE);
                                panelSelectCategories.setVisibility(View.GONE);
                                return true;
                            case R.id.menu_user_profile:
                                panelHome.setVisibility(View.GONE);
                                panelProfile.setVisibility(View.VISIBLE);
                                panelSeeds.setVisibility(View.GONE);
                                panelLeaderboard.setVisibility(View.GONE);
                                panelSelectCategories.setVisibility(View.GONE);

                                loadUserDetsInProfile();

                                return true;
                            case R.id.menu_user_seeds:
                                panelHome.setVisibility(View.GONE);
                                panelProfile.setVisibility(View.GONE);
                                panelSeeds.setVisibility(View.VISIBLE);
                                panelLeaderboard.setVisibility(View.GONE);
                                panelSelectCategories.setVisibility(View.GONE);

                                ArrayList<InfoPurchaseSeeds> arrayOfSeedAmounts = new ArrayList<InfoPurchaseSeeds>();
                                adapter = new AdapterPurchaseSeeds(ScreenHome.this,
                                        arrayOfSeedAmounts);
                                for(int i =0; i<InterfacePurchaseSeeds.seedsAmounts.length;i++){
                                    newSeedInfo = new InfoPurchaseSeeds
                                            (InterfacePurchaseSeeds.seedsAmounts[i],
                                                    InterfacePurchaseSeeds.seedsPrices[i]);
                                    adapter.add(newSeedInfo);
                                }
                                ListView lstVwPurchaseSeeds = findViewById(R.id.lstViewPurchaseSeeds);
                                lstVwPurchaseSeeds.setAdapter(adapter);
                                return true;
                            case R.id.menu_user_level:
                                panelHome.setVisibility(View.GONE);
                                panelProfile.setVisibility(View.GONE);
                                panelSeeds.setVisibility(View.GONE);
                                panelLeaderboard.setVisibility(View.VISIBLE);
                                panelSelectCategories.setVisibility(View.GONE);

                                Toast.makeText(ScreenHome.this, "Select the Image to see more...", Toast.LENGTH_LONG).show();

                                loadStaticLeaderboard();
                                initRecyclerViewLeadboard();

                                txtDispLeaderboardList.setText("Choose A Leaderboard");

                                return true;
                        }
                        return true;
                    }
                });

        DatabaseReference checkUserOnboardingRef = Constants.myUsersOnboardingRef;
        checkUserOnboardingRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    //do nothing
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(ScreenHome.this);
                    builder.setMessage("Please note that we require you to complete the Onboarding process.")
                            .setPositiveButton("I understand", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    startActivity(new Intent(ScreenHome.this,
                                            ScreenOnboarding.class));
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        loadHomeCat();


        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("leaderboard-item"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiverLevel,
                new IntentFilter("leaderboard-level"));

        //This still needs to be looked at, please look at the bottom for the method
        /*LocalBroadcastManager.getInstance(this).registerReceiver(mMessageHomeCat,
                new IntentFilter("home-cat-item"));*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_screen_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(ScreenHome.this);
                builder.setCancelable(true);
                builder.setMessage("Are you sure you want to logout?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                firebaseSignOut();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {}
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            case R.id.settings_developer:
                Intent browserIntent =
                        new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.swiidappdesigns.com"));
                startActivity(browserIntent);
                return true;
            case R.id.settings_create_event:
                startActivity(new Intent(ScreenHome.this, ScreenCreateEvent.class));
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    //The following section is for the buttons
    public void profileBuySeedsSelected(View view){
        panelProfile.setVisibility(View.GONE);
        panelSeeds.setVisibility(View.VISIBLE);
        botNavView.setSelectedItemId(R.id.menu_user_seeds);
    }
    public void backToLeaderboardSelected(View view){
        initRecyclerViewLeadboard();
        btnBackToLeaderboard.setVisibility(View.GONE);
        txtDispLeaderboardList.setText("Choose A Leaderboard");
    }
    public void backToLevelSelected(View view){
        availableUserName.clear();
        availableUserImgURLs.clear();
        availableUserPoints.clear();
        initRecyclerViewLeadboardLevels();
        crdVwLeaderboardUsers.setVisibility(View.GONE);
        crdVwLeaderboard.setVisibility(View.VISIBLE);
    }
    public void editProfileSelected(View view){
        startActivity(new Intent(ScreenHome.this, ScreenEditProfile.class));
    }
    public void selectCategoriesSelected(View view){
        panelSelectCategories.setVisibility(View.VISIBLE);
        panelProfile.setVisibility(View.GONE);
        loadCategoriesOwned();
    }

    //The following section is for the loading of static images/names
    private void loadStaticImagesAndNames(){
        availableImgURLs.add("http://s3.india.com/wp-content/uploads/2014/09/diego-csota.jpg");
        availableSubCat.add("Barclays Premier League");

        availableImgURLs.add("http://hanabi.autoweek.com/sites/default/files/styles/gen-932-524/public/_X4I8647.jpg?itok=lqhK9ttA");
        availableSubCat.add("Formula 1");

        availableImgURLs.add("http://cdn.primedia.co.za/primedia-broadcasting/image/upload/c_fill,h_289,q_70,w_463/x28ov8yepwk7yyndhnjv");
        availableSubCat.add("Currie Cup");
    }
    private void loadStaticLeaderboard(){
        availableLeaderboardItems.add("General Leaderboard");
        availableLeaderboardItemsImgURLs.add("https://png.icons8.com/ios/1600/unchecked-circle.png");

        availableLeaderboardItems.add("Friends Leaderboard");
        availableLeaderboardItemsImgURLs.add("https://png.icons8.com/ios/1600/unchecked-circle.png");
    }
    private void loadStaticLevels(){
        for(int i = 0; i < Constants.arrayLevels.length;i++){
            availableLevels.add(Constants.arrayLevels[i]);
            availableLevelsImgURLs.add("https://png.icons8.com/ios/1600/unchecked-circle.png");
        }
    }
    private void loadUserDetsInProfile(){
        DatabaseReference userDetFound = Constants.myUsersPerDetRef;
        userDetFound.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String value = snapshot.getKey().toString();
                        if(value.equals("profileName")){
                            String valueFound = snapshot.getValue().toString();
                            txtDispUserName.setText(valueFound);
                        }else if(value.equals("profilePic")){
                            String valueFound = snapshot.getValue().toString();
                            Glide.with(getApplication()).load(valueFound).into(imgUserProfilePic);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        DatabaseReference userLevelFound = Constants.myUsersLevelRef;
        userLevelFound.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String value = snapshot.getKey().toString();
                        if(value.equals("userLevelName")){
                            String valueFound = snapshot.getValue().toString();
                            txtUserLevel.setText(valueFound);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        DatabaseReference userCreditFound = Constants.myUsersCreditBalRef;
        userCreditFound.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String value = snapshot.getKey().toString();
                        if(value.equals("creditBalance")){
                            String valueFound = snapshot.getValue().toString();
                            txtUserSeeds.setText(valueFound);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }
    private void loadCategoriesOwned(){
        DatabaseReference userDetFound = Constants.myUsersinterestsRef;
        userDetFound.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String value = snapshot.getKey().toString();
                        if(value.equals("interestMainCat1")){
                            String valueFound = snapshot.getValue().toString();
                            availableUserOwnedMainCat.add(valueFound);
                            availableUserOwnedMainCatImg.add("https://png.icons8.com/ios/1600/unchecked-circle.png");
                        }else if(value.equals("interestMainCat2")){
                            String valueFound = snapshot.getValue().toString();
                            availableUserOwnedMainCat.add(valueFound);
                            availableUserOwnedMainCatImg.add("https://png.icons8.com/ios/1600/unchecked-circle.png");
                        }
                        else if(value.equals("interestMainCat3")){
                            String valueFound = snapshot.getValue().toString();
                            availableUserOwnedMainCat.add(valueFound);
                            availableUserOwnedMainCatImg.add("https://png.icons8.com/ios/1600/unchecked-circle.png");
                        }else if(value.equals("interestMainCat4")){
                            String valueFound = snapshot.getValue().toString();
                            availableUserOwnedMainCat.add(valueFound);
                            availableUserOwnedMainCatImg.add("https://png.icons8.com/ios/1600/unchecked-circle.png");
                        }else if(value.equals("interestMainCat5")){
                            String valueFound = snapshot.getValue().toString();
                            availableUserOwnedMainCat.add(valueFound);
                            availableUserOwnedMainCatImg.add("https://png.icons8.com/ios/1600/unchecked-circle.png");
                        }
                    }
                    initRecyclerViewSelectMainCat();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }
    private void loadHomeCat(){
        for(int i = 0; i < Constants.arrayMainCatNames.length; i++){
            availableHomeCat.add(Constants.arrayMainCatNames[i]);
            availableHomeCatImgURL.add(R.drawable.ic_seed);
        }
        initRecyclerViewHomeCat();
    }

    //The following section is for the init of the recycler views
    private void initRecyclerViewSubCat(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false);
        RecyclerView recyclerView = findViewById(R.id.recyclerViewSubCat);
        recyclerView.setLayoutManager(layoutManager);
        AdapterRecycleSubCat adapter = new AdapterRecycleSubCat(availableSubCat,availableImgURLs,this);
        recyclerView.setAdapter(adapter);
    }
    private void initRecyclerViewHighlights(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false);
        RecyclerView recyclerViewHighlights = findViewById(R.id.recyclerViewHighlights);
        recyclerViewHighlights.setLayoutManager(layoutManager);
        AdapterRecycleHighlights adapterHighlights = new AdapterRecycleHighlights(availableSubCat,availableImgURLs,this);
        recyclerViewHighlights.setAdapter(adapterHighlights);
    }
    private void initRecyclerViewLeadboard(){
        layoutManagerLeaderboard = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        recyclerViewLeaderboard = findViewById(R.id.recyclerViewLeaderboard);
        recyclerViewLeaderboard.setLayoutManager(layoutManagerLeaderboard);
        adapterLeaderboard = new AdapterRecycleLeaderboard(availableLeaderboardItems,availableLeaderboardItemsImgURLs,this);
        recyclerViewLeaderboard.setAdapter(adapterLeaderboard);
    }
    private void initRecyclerViewLeadboardLevels(){
        layoutManagerLeaderboardLevels = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        recyclerViewLeaderboardLevels = findViewById(R.id.recyclerViewLeaderboard);
        recyclerViewLeaderboardLevels.setLayoutManager(layoutManagerLeaderboardLevels);
        adapterLeaderboardLevels = new AdapterRecycleLeaderboardLevels(availableLevels,availableLevelsImgURLs,this);
        recyclerViewLeaderboardLevels.setAdapter(adapterLeaderboardLevels);
    }
    private void initRecyclerViewUsersInLevels(){
        layoutManagerUserInLevels = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        recyclerViewUsersInLevels = findViewById(R.id.recyclerViewLeaderboardUsers);
        recyclerViewUsersInLevels.setLayoutManager(layoutManagerUserInLevels);
        adapaterUsersInLevels = new AdapterRecycleUsersInLevels(availableUserName,
                availableUserImgURLs,availableUserPoints,this);
        recyclerViewUsersInLevels.setAdapter(adapaterUsersInLevels);
    }
    private void initRecyclerViewSelectMainCat(){
        layoutManagerSelectMainCat = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        recyclerViewSelectMainCat = findViewById(R.id.recyclerViewCategoriesSelectOwned);
        recyclerViewSelectMainCat.setLayoutManager(layoutManagerSelectMainCat);
        adapaterSelectMainCat = new AdapterRecycleSelectMainCat(availableUserOwnedMainCat,availableUserOwnedMainCatImg,this);
        recyclerViewSelectMainCat.setAdapter(adapaterSelectMainCat);
    }
    private void initRecyclerViewHomeCat(){
        layoutManagerHomeCat = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false);
        recyclerViewHomeCat = findViewById(R.id.recyclerViewDispHomeCat);
        recyclerViewHomeCat.setLayoutManager(layoutManagerHomeCat);
        adapaterHomeCat = new AdapterRecycleHomeCat(availableHomeCat,availableHomeCatImgURL,this);
        recyclerViewHomeCat.setAdapter(adapaterHomeCat);
    }

    //The following section is to do with the local broadcasts
    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            sItemName = intent.getStringExtra("leaderboardSelected");
            if(sItemName.equals("General Leaderboard")){
                loadStaticLevels();
                initRecyclerViewLeadboardLevels();
                btnBackToLeaderboard.setVisibility(View.VISIBLE);
                txtDispLeaderboardList.setText(sItemName);
            }
        }
    };
    public BroadcastReceiver mMessageReceiverLevel = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            crdVwLeaderboard.setVisibility(View.GONE);
            crdVwLeaderboardUsers.setVisibility(View.VISIBLE);
            sItemNameLevel = intent.getStringExtra("levelSelected");
            txtDispLeaderboardLevelsList.setText(sItemNameLevel);
            DatabaseReference checkUsersInLevelRef = Constants.leaderboardGeneralRef
                    .child(sItemNameLevel);
            checkUsersInLevelRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            userID = snapshot.getKey().toString();
                            String userPoints = snapshot.getValue().toString();
                            availableUserPoints.add(userPoints);
                            DatabaseReference checkUserNameRef = Constants.myUsersRef
                                    .child(userID)
                                    .child("userPersonalDet")
                                    .child("profileName");
                            checkUserNameRef.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists()){
                                        String value = dataSnapshot.getValue().toString();
                                        availableUserName.add(value);
                                        DatabaseReference checkUserImgRef = Constants.myUsersRef
                                                .child(userID)
                                                .child("userPersonalDet")
                                                .child("profilePic");
                                        checkUserImgRef.addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if(dataSnapshot.exists()){
                                                    String value = dataSnapshot.getValue().toString();
                                                    availableUserImgURLs.add(value);
                                                    initRecyclerViewUsersInLevels();
                                                }
                                            }
                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {}
                                        });
                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {}
                            });
                        }
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }
    };

    //This section still needs to be looked at
    /*public BroadcastReceiver mMessageHomeCat = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            sItemName = intent.getStringExtra("homeCatSelected");
            DatabaseReference userDetFound = Constants.myUsersinterestsRef;
            userDetFound.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            String value = snapshot.getKey().toString();
                            Toast.makeText(ScreenHome.this, "value: " + value, Toast.LENGTH_SHORT).show();
                            for(int i = 0; i < Constants.arrayMainCatDB.length; i++){
                                Toast.makeText(ScreenHome.this, "arrayMainCatDB: " + Constants.arrayMainCatDB[i], Toast.LENGTH_SHORT).show();
                                if(Constants.arrayMainCatDB[i].equals(value)){
                                    String valueFound = snapshot.getValue().toString();
                                    Toast.makeText(ScreenHome.this, "valueFound: " + valueFound, Toast.LENGTH_SHORT).show();
                                    for(int j = 0; j < Constants.arrayMainCatNames.length; j++){
                                        Toast.makeText(ScreenHome.this, "arrayMainCatNames: " + Constants.arrayMainCatNames[j], Toast.LENGTH_SHORT).show();
                                        if(Constants.arrayMainCatNames[j].equals(valueFound)){
                                            panelHomeCatOwned.setVisibility(View.VISIBLE);
                                            panelHomeCatNotOwned.setVisibility(View.GONE);
                                        }else{
                                            panelHomeCatOwned.setVisibility(View.GONE);
                                            panelHomeCatNotOwned.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }
    };*/

    private void firebaseSignOut(){
        AuthUI.getInstance()
                .signOut(ScreenHome.this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        // user is now signed out
                        startActivity(new Intent(ScreenHome.this,
                                ScreenSplash.class));
                        finish();
                    }
                });
    }

}
